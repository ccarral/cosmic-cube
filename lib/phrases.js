
const PHRASES = ['Menos es más', 'Una crisis a la vez', 'No se puede todo'];

exports.getRandomPhrase = () => {
  const random = Math.floor(Math.random()*PHRASES.length);
  return PHRASES[random];
};

