exports.navBar = [ {
  title: 'Software',
  href: '/software',
  testId: 'software'
},
{
  title: 'Blog',
  href: '/blog',
  testId: 'blog'
},
{
  title: 'Música',
  href: '/music',
  testId: 'music'
},
{

  title: 'Acerca del sitio',
  href: '/about',
  testId: 'about'
}
];
