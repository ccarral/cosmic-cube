const handlers = require('../handlers');

test('Home page renders', () =>{
  const req = {};
  const res = { render: jest.fn()};
  handlers.home(req,res);
  expect(res.render.mock.calls[0][0]).toBe('home');
});

test('About page renders with phrase',() =>{
  const req = {};
  const res = {
    render: jest.fn()
  };
  handlers.about(req,res);
  expect(res.render.mock.calls.length).toBe(1);
  expect(res.render.mock.calls[0][0]).toBe('about');
  expect(res.render.mock.calls[0][1]).toEqual(expect.objectContaining({
    phrase: expect.stringMatching(/\W/),
  }));
});

test('Music page renders succesfully', () =>{
  const req = {};
  const res = {
    render: jest.fn()
  };

  handlers.music(req,res);
  expect(res.render.mock.calls[0][0]).toBe('music');
});


test('404 handler', ()=>{
  const req = {};
  const res = {
    render:jest.fn(),
    status:jest.fn()
  };

  handlers.notFound(req,res);
  expect(res.render.mock.calls.length).toBe(1);
  expect(res.render.mock.calls[0][0]).toBe('404');
  expect(res.status.mock.calls[0][0]).toBe(404);
  
});

test('505 handler', ()=>{
  const err = new Error('Some error');
  const req = {};
  const res = {
    render : jest.fn(),
    status: jest.fn()
  };

  const next = jest.fn();
  handlers.serverError(err,req,res,next);
  expect(res.render.mock.calls.length).toBe(1);
  expect(res.render.mock.calls[0][0]).toBe('500');
});
