const blogHandler = require('../blog');

const ENTRIES_METADATA = 'tests/mocks/blog/entries/metadata';
const ENTRIES_BODY = 'tests/mocks/blog/entries/body/';


test('Read entry data', () =>{

  const metadataArray = blogHandler.fetchEntriesMetadata(ENTRIES_METADATA);

  expect(metadataArray.length).toBe(2);
  expect(metadataArray[0].title).toBe('Title');
  expect(metadataArray[1].title).toBe('Title 2');

  blogHandler.fetchEntriesBody(ENTRIES_BODY, metadataArray);

  expect(typeof metadataArray[0].body).toBe('string');
  expect(typeof metadataArray[1].body).toBe('string');

});
