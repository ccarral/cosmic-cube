const { credentials } = require('../config.js');
const SpotifyWebApi = require('spotify-web-api-node');

const COSMIC_CUBE_PLAYLIST_ID = '3RjHBD69THydLiYqlCPVFm';


const getPlaylistData = (playlistID) =>{

  let {clientId, clientSecret} = credentials.spotify;

  let spotifyApi = new SpotifyWebApi({
    clientId:clientId,
    clientSecret:clientSecret
  });

  return new Promise( (resolve, reject)=>{
    spotifyApi.clientCredentialsGrant().then(
      (data) =>{
        console.log('Spotify access granted to app');
        console.log(`Access Token: ${data.body['access_token']}`);
        spotifyApi.setAccessToken(data.body['access_token']);
        spotifyApi.getPlaylistTracks(playlistID).then(
          (data) =>{
            const tracks = getTrackList(data.body.items);
            resolve(tracks);
          },
          (err) =>{
            console.log('Error while fetching playlist items!');
            // console.log(err.stack);
            reject(err);
          });
      },
      (err) =>{
        console.log('Error while granting credentials');
        reject(err);
      });

  });


};

const getTrackList = (data) =>{
  const trackList = data.map(( item ) =>{
    let newItem = {
      thumbnailUrl : item.track.album.images[0].url,

      artists : item.track.album.artists.map( ( artist) =>{
        const artistItem = {
          name: artist.name,
          url: artist.external_urls.spotify
        };
        return artistItem;
      }),

      name: item.track.name,

      trackUrl: item.track.external_urls.spotify,

      albumUrl: item.track.album.external_urls.spotify
    };

    return newItem;
        
  });

  return trackList;
};

exports.getMusicList = (callback) =>{
  getPlaylistData(COSMIC_CUBE_PLAYLIST_ID).then((data) =>{
    data.forEach( (track) =>{
      track['author'] = track.artists[0].name;
    });
    callback(data);
  }).catch( (err) => console.log(err));


};
