const phrase = require('./phrases.js');
const music = require('./music.js');
const blog = require('./blog.js');
const widgets = require('./widgets.js');

exports.home = (req,res) =>{

  res.render('home',
    {
      title: 'Cubo Cósmico',
      navBar: widgets.navBar
    });
};

exports.blog = (req,res) =>{

  blog.getAllPosts((result) =>{
    res.render('blog',{
      title: 'Blog',
      entries: result.rows,
      navBar: widgets.navBar
    });


  });

};

exports.about = (req,res) => {
  res.render('about', {
    title: 'Acerca del sitio',
    phrase : phrase.getRandomPhrase(),
    navBar: widgets.navBar,
    extraStyleSheet: '/styles/about.css'

  });

};

exports.api = {
  sendMessage : (req, res) =>{
    console.log(req.body.message);
    res.send({ result: 'success' });
  }

};


exports.music = (req,res) =>{

  music.getMusicList( (list) =>{
    console.log(list);
    res.render('music',{

      title: 'Música',
      musicList: list,
      navBar: widgets.navBar,
      extraStyleSheet: '/styles/music.css'
    });

  }); 
};

exports.software = (req,res) =>{
  res.render('software',{
    title: 'Software',
    navBar: widgets.navBar
  });
};

exports.notFound = (req,res) => {
  res.status(404);
  res.render('404');
};

exports.serverError = (err,req,res,next) => {
  console.log(err.stack);
  res.status(500);
  res.render('500');
};
