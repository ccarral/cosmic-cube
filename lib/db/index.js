const { Pool} = require('pg');
const { credentials } = require('../../config.js');
const dbMetadata = require('./metadata.js');

exports.dbMetadata = dbMetadata;

const connectionData  = credentials.postgres;
const connectionString = connectionData.connectionString;

const pool = new Pool({
  connectionString
});


exports.testNow = () =>{
  pool.query('SELECT NOW()', (err, res)=>{
    console.log(connectionData);
    console.log(err,res);
  });
};

exports.dbQuery = async (text,params,callback) =>{
  return pool.query(text,params,callback);
};

