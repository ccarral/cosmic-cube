const env = process.env.NODE_ENV || 'dev';

const metadata = require(`../../resources/db-metadata.${env}`);

module.exports = { metadata };
