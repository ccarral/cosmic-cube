const express = require('express');
const expressHandlebars = require('express-handlebars');
const handlers = require('./lib/handlers.js');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');

const app = express();

app.engine('handlebars', expressHandlebars({
  defaultLayout : 'main'
}));

app.use(bodyParser.json());



app.set('view engine', 'handlebars');

const port = process.env.PORT || 8000;

app.use(express.static(__dirname+'/public'));


app.get('/',handlers.home);

app.get('/about',handlers.about);

app.post('/api/send-message',handlers.api.sendMessage);

app.get('/blog', handlers.blog);

app.get('/music', handlers.music);


app.get('/software', handlers.software);

app.use(handlers.notFound);

app.use(handlers.serverError);

if(require.main == module){
  app.listen(port, () => console.log(
    `Express started on http://localhost:${port};`+
    'Press Ctrl-c to terminate'));
}else{
  module.exports = app;
}


